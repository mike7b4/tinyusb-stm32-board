# TinyUSB STM32 Board

Try make it simple...

## Parts

 |Part     |Value| Sum |
 |---------|-----|-----|
 |stm32f042|32kB |1    |
 |MCP1700T |3.3v |1    |
 |Resistor |10k  |?    |
 |Resistor |53,3k|1    |
 |Capacitor|10nF |1    |
 |Capacitor|100nF|2    |
 |Capacitor|1uF  |2    |
 |Led      |blue |1    |
 |PFET     |TBD  |1    |
 |Terminal |2pin |1    |
  

